require stream
require hfc8

epicsEnvSet("P", "REMS-FEBRS:")
epicsEnvSet("R", "RMT-SHM-001:")
epicsEnvSet("STREAM_PROTOCOL_PATH","$(hfc8_DB)")

epicsEnvSet("HOSTNAME","rems-shm-001.tn.esss.lu.se")

epicsEnvSet("HFC8_RELAY_HOST","localhost")
epicsEnvSet("HFC8_RELAY_PORT",12345)

iocshLoad $(hfc8_DIR)/hfc8.iocsh "HOSTNAME=$(HOSTNAME), HFC8_RELAY_HOST=$(HFC8_RELAY_HOST), HFC8_RELAY_PORT=$(HFC8_RELAY_PORT), P=$(P), R=$(R)"

iocInit()

